# Binding tools for soft button

Could be used for any key

Based on the [sxhkd project](https://github.com/baskerville/sxhkd)

## Installation
1. Install dependencies
```bash
sudo apt-get install sxhkd
sudo apt-get install ffmpeg # play mp3 sounds
```

2. Setup profile
Add the following to the `~/.profile` (CHANGE NECCESSARY VARIABLES): 
```
sxhkd & # Run keys binder
SOFT_BUTTON_PROJECT_PATH=home/maksymv/Documents/Projects/soft_button_binding
```

3. Create sxhkd configs:
```
mkdir ~/.config/sxhkd && vim ~/.config/sxhkd/sxhkdrc
```
And paste content of `sxhkdrc_example` file. Do not forget to change hardcoded path
in configs, as it can't use variables or relative path

4. Start listening wee wee keys (Run each time after reboot, or setup autorun):
```
bash restart_sxhkd.sh
```