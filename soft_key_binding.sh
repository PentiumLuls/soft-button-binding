sound() {
	ffplay -nodisp -autoexit $1
}

do_weewee() {
	# TODO: add env for different sound packs
	PATH_TO_SOUND_PACK=sounds/gachi/
	RANDOM_SOUND=$(find $PATH_TO_SOUND_PACK -type f | shuf -n 1)
	sound $RANDOM_SOUND
}

do_weewee
